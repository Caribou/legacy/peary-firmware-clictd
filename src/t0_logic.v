`timescale 1ns / 1ps

module t0_filter_logic #(
    parameter DATAWIDTH   = 10  
  )(
    input wire clk,
    input wire dcm_locked,
    input wire [DATAWIDTH-1:0] paralel_in,
    output wire signal_out,
    output wire serdes_reset_out
  );
  

wire signal_out_i;
reg [4:0] delay;
wire delay_rst;

assign serdes_reset_out = delay_rst;
assign signal_out = signal_out_i;
assign signal_out_i = &paralel_in;

assign delay_rst = |delay;

always @(posedge clk) begin
  if (!dcm_locked) begin
    delay <= 5'b11111;
  end
  else if (delay_rst) begin
    delay <= delay - 1;
  end
end

endmodule
